/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx  = 1;        /* border pixel of windows */
static const unsigned int snap      = 32;       /* snap pixel */
static const int showbar            = 1;        /* 0 means no bar */
static const int topbar             = 1;        /* 0 means bottom bar */
static const char *fonts[]          = { "Terminus:size=12" };
static const char col_green[]       = "#00FF00";
static const char col_lightgray[]   = "#eeeeee";
static const char col_black[]	    = "#000000";
static const char *colors[][3]      = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_green, col_black, col_black },
	[SchemeSel]  = { col_lightgray, col_black,  col_green  },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class                      instance          title         tags mask     isfloating   monitor */
	{ "Nextcloud",                NULL,             NULL,         1 << 8,       1,           -1 },
	{ "st",                       "floating",       NULL,         0,            1,           -1 },
	{ "Qemu-system-i386",         NULL,             NULL,         1 << 1,       0,           -1 },
	{ "alacritty_floating",       NULL,             NULL,              0,       1,           -1 },
	{ "odin-term",                NULL,             NULL,         1 << 1,       1,           -1 },
};

/* layout(s) */
static const float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 1;    /* 1 means respect size hints in tiled resizals */

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[]=",      tile },    /* first entry is default */
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ "[M]",      monocle },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[]    = { "dmenu_run", NULL };

static const char *termcmd[]     = { "alacritty", NULL };
static const char *ftermcmd[]    = { "alacritty", "--class", "alacritty_floating", NULL };

static const char *webcmd[]        =  { "firefox", NULL };
static const char *privwebcmd[]    =  { "firefox", "--private-window", NULL };
static const char *webcmd2[]       =  { "chromium", NULL };
static const char *privwebcmd2[]   =  { "chromium", "--incognito", NULL };
static const char *xcmd[]          =  { "pkill", "x", NULL };
static const char *mutecmd[]       =  { "pulsemixer", "--toggle-mute", NULL };
static const char *volcmd[]        =  { "alacritty", "--command", "pulsemixer", NULL };
static const char *lockcmd[]       =  { "slock", NULL };
static const char *filecmd[]       =  { "alacritty", "--command", "nnn", "-Rd", NULL };
static const char *ffilecmd[]      =  { "alacritty", "--class", "alacritty_floating",  "--command", "nnn", "-Rd", NULL };
static const char *musiccmd[]      =  { "alacritty", "--command", "musikcube", NULL };
static const char *ssacmd[]        =  { "ssall", NULL };
static const char *ssscmd[]        =  { "ssselection", NULL };
static const char *passcmd[]       =  { "keepassxc", NULL };
static const char *vncviewercmd[]  =  { "vncviewer", NULL };
static const char *clipmenucmd[]   =  { "clipmenu", NULL };

static Key keys[] = {
	/* modifier          key        function        argument */
	{ MODKEY,            XK_d,      spawn,          {.v = dmenucmd } },
	{ MODKEY|ShiftMask,  XK_Return, spawn,          {.v = termcmd } },
	{ MODKEY|ControlMask,XK_Return, spawn,          {.v = ftermcmd } },
	{ MODKEY,            XK_b,      togglebar,      {0} },
	{ MODKEY,            XK_j,      focusstack,     {.i = +1 } },
	{ MODKEY,            XK_k,      focusstack,     {.i = -1 } },
	{ MODKEY,            XK_i,      incnmaster,     {.i = +1 } },
	{ MODKEY,            XK_o,      incnmaster,     {.i = -1 } },
	{ MODKEY,            XK_h,      setmfact,       {.f = -0.05} },
	{ MODKEY,            XK_l,      setmfact,       {.f = +0.05} },
	{ MODKEY,            XK_Return, zoom,           {0} },
	{ MODKEY,            XK_Tab,    view,           {0} },
	{ MODKEY|ShiftMask,  XK_q,      killclient,     {0} },
	{ MODKEY,            XK_t,      setlayout,      {.v = &layouts[0]} },
	{ MODKEY,            XK_f,      setlayout,      {.v = &layouts[1]} },
	{ MODKEY,            XK_m,      setlayout,      {.v = &layouts[2]} },
	{ MODKEY,            XK_space,  setlayout,      {0} },
	{ MODKEY|ShiftMask,  XK_space,  togglefloating, {0} },
	{ MODKEY,            XK_0,      view,           {.ui = ~0 } },
	{ MODKEY|ShiftMask,  XK_0,      tag,            {.ui = ~0 } },
	{ MODKEY,            XK_comma,  focusmon,       {.i = -1 } },
	{ MODKEY,            XK_period, focusmon,       {.i = +1 } },
	{ MODKEY|ShiftMask,  XK_comma,  tagmon,         {.i = -1 } },
	{ MODKEY|ShiftMask,  XK_period, tagmon,         {.i = +1 } },
	TAGKEYS(             XK_1,                      0)
	TAGKEYS(             XK_2,                      1)
	TAGKEYS(             XK_3,                      2)
	TAGKEYS(             XK_4,                      3)
	TAGKEYS(             XK_5,                      4)
	TAGKEYS(             XK_6,                      5)
	TAGKEYS(             XK_7,                      6)
	TAGKEYS(             XK_8,                      7)
	TAGKEYS(             XK_9,                      8)
	{ MODKEY,            XK_x,      quit,           {0} },

	{ MODKEY|ShiftMask,  XK_x,      spawn,          {.v = xcmd } },
	{ MODKEY,            XK_z,      spawn,          {.v = lockcmd } },

	{ MODKEY,            XK_q,      spawn,          {.v = webcmd } },
	{ MODKEY|ShiftMask,  XK_p,      spawn,          {.v = privwebcmd } },

	{ MODKEY,            XK_w,      spawn,          {.v = webcmd2 } },
	{ MODKEY|ShiftMask,  XK_w,      spawn,          {.v = privwebcmd2 } },

	{ MODKEY,            XK_r,      spawn,          {.v = filecmd } },
	{ MODKEY|ShiftMask,  XK_r,      spawn,          {.v = ffilecmd } },

	{ MODKEY,            XK_c,      spawn,          {.v = mutecmd } },
	{ MODKEY,            XK_v,      spawn,          {.v = volcmd } },
	{ MODKEY|ShiftMask,  XK_v,      spawn,          {.v = musiccmd } },

	{ MODKEY,            XK_s,      spawn,          {.v = ssacmd } },
	{ MODKEY|ShiftMask,  XK_s,      spawn,          {.v = ssscmd } },

	{ MODKEY,            XK_y,      spawn,          {.v = vncviewercmd } },

	{ MODKEY,            XK_p,      spawn,          {.v = passcmd } },

	{ MODKEY|ShiftMask,  XK_d,      spawn,          {.v = clipmenucmd } },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button2,        spawn,          {.v = termcmd } },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

